﻿namespace Scripting.Python
{
    using Microsoft.Scripting;
    using Microsoft.Scripting.Hosting; 

    public class PythonEngine : Scripting.Hosting.ScriptEngine
    {
        public override string Name
        {
            get { return engine.Setup.DisplayName; }
        }

        private ScriptEngine engine;

        public PythonEngine()
        {
            engine = IronPython.Hosting.Python.CreateEngine();
        }

        public override Scripting.Hosting.ScriptSession CreateSession(object hostObject)
        {
            return new PythonSession();
        }

        public override object Execute(string content)
        {
            return engine.Execute(content);
        }

        public override T Execute<T>(string content)
        {
            return engine.Execute<T>(content);
        }
    }
}
