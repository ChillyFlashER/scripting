﻿namespace Scripting.Python
{
    public class PythonSession : Scripting.Hosting.ScriptSession
    {
        private Microsoft.Scripting.Hosting.ScriptEngine scriptEngine;
        private Microsoft.Scripting.Hosting.ScriptScope scriptScope;

        internal PythonSession()
        {
            scriptEngine = IronPython.Hosting.Python.CreateEngine();
            scriptScope = scriptEngine.CreateScope();
        }

        public override object Execute(string content)
        {
            return scriptEngine.Execute(content, scriptScope);
        }

        public override T Execute<T>(string content)
        {
            return scriptEngine.Execute<T>(content, scriptScope);
        }
    }
}
