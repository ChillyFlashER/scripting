﻿namespace Script
{
    using Jurassic;
    using Jurassic.Library;
    using System;

    // TODO: Expose Methods + Static Methods

    public class JavascriptExpose
    {
        internal object Value;
        internal JExpose expose;

        internal Javascript Javascript
        {
            get { return javascript; }
            set
            {
                if (javascript == null)
                {
                    javascript = value;
                    expose = new JExpose(javascript.engine);
                    UpdateAllProperties();
                }
            }
        }
        private Javascript javascript;

        public JavascriptExpose(object value)
        {
            this.Value = value;
        }

        public void UpdateAllProperties()
        {
            var properties = Value.GetType().GetProperties();
            foreach (var prop in properties)
            {
                UpdateProperty(prop.Name);
            }
        }

        public void UpdateProperty(string propertyName)
        {
            if (expose == null)
                throw new ArgumentNullException();

            var property = Value.GetType().GetProperty(propertyName);

            if (property == null)
                throw new ArgumentNullException("There is no property named " + propertyName);

            var attributes = property.CanWrite ? PropertyAttributes.Writable : PropertyAttributes.Sealed;

            expose.DefineProperty(propertyName, 
                new PropertyDescriptor(property.GetValue(Value), attributes), false);
        }

        #region Method

        public bool Delete(string propertyName, bool throwOnError)
        {
            if (expose == null)
                throw new ArgumentNullException();

            return expose.Delete(propertyName, throwOnError);
        }

        public object GetPropertyValue(string propertyName)
        {
            if (expose == null)
                throw new ArgumentNullException();

            return expose.GetPropertyValue(propertyName);
        }

        public void SetPropertyValue(string propertyName, object value, bool throwOnError)
        {
            if (expose == null)
                throw new ArgumentNullException();

            expose.SetPropertyValue(propertyName, value, throwOnError);
        }

        #endregion

        internal class JExpose : ObjectInstance
        {
            public JExpose(ScriptEngine engine)
                : base(engine)
            {

            }
        }
    }
}
