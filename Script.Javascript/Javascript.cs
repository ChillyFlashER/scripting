﻿namespace Script
{
    using Jurassic;
    using Jurassic.Library;
    using System;

    /// <summary>
    /// A Wrapper for Jurassic.
    /// </summary>
    public class Javascript : SScript
    {
        #region Properties

        public override string Name
        {
            get { return "Javascript"; }
        }

        #endregion

        #region Fields

        internal ScriptEngine engine;
        internal FirebugConsole console;

        #endregion

        #region Contructors

        public Javascript()
        {
            engine = new ScriptEngine();
            engine.SetGlobalValue("console", console = new FirebugConsole(engine));

#if DEBUG
            engine.EnableDebugging = true;
#endif
        }

        #endregion

        #region Methods

        public void SetGlobalFunction(string functionName, Delegate functionDelegate)
        {
            engine.SetGlobalFunction(functionName, functionDelegate);
        }

        public bool HasGlobalValue(string variableName)
        {
            return engine.HasGlobalValue(variableName);
        }

        public object GetGlobalValue(string variableName)
        {
            return engine.GetGlobalValue(variableName);
        }

        public T GetGlobalValue<T>(string variableName)
        {
            return engine.GetGlobalValue<T>(variableName);
        }

        public void SetGlobalValue(string variableName, object value)
        {
            engine.SetGlobalValue(variableName, value);
        }

        public void SetGlobalValue(string variableName, JavascriptExpose value)
        {
            value.Javascript = this;
            engine.SetGlobalValue(variableName, value.expose);
        }

        #endregion

        #region Override Methods

        public override void CreateMethod(string content)
        {
            engine.Execute(content);
        }

        public override T Run<T>(string content)
        {
            return engine.Evaluate<T>(content);
        }

        public override T RunMethod<T>(string method, params object[] parameters)
        {
            return engine.CallGlobalFunction<T>(method, parameters);
        }

        #endregion
    }
}
