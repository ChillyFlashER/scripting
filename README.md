# Scripting

## Overview

Exploring Scripting with CSharp. And creating a general api for every language so it is easy to use everywhere. 
> This is created for possibly future use in [Engine Nine](https://nine.codeplex.com)

## Languages

* CSharp (C#)
* VisualBasic

#### Extendable:

* Javascript
* Python
* Ruby
* Scheme

### What should every language be able to do?

* Create Objects
* Store Objects
* Interact with Objects
* Interact with a Host

## Extensibility
	
You should be able to add your own api and limit what is added.

***

| Features          | [Roslyn] | [Jurassic] | [IronPython] | [IronRuby] | [IronScheme] |
| :---              | :---: | :---: | :---: | :---: | :---: |
| HostObject        | x |   |   |   |	|
| Code Cache        | x |   | x | x | x |
|                   |   |   |   |   |   |










[Roslyn]: http://msdn.microsoft.com/en-us/vstudio/roslyn.aspx
[Jurassic]: https://jurassic.codeplex.com/
[IronPython]: http://ironpython.net/
[IronRuby]: http://ironruby.net/
[IronScheme]: https://ironscheme.codeplex.com/