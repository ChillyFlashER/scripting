﻿namespace Scripting.CSharp
{
    using Scripting.Hosting;

    public class CSharpSession : ScriptSession
    {
        #region Properties

        public Roslyn.Scripting.CSharp.ScriptEngine Engine
        {
            get { return engine; }
        }
        internal Roslyn.Scripting.CSharp.ScriptEngine engine;

        internal Roslyn.Scripting.Session session;

        #endregion

        public void AddReference(System.Reflection.Assembly assembly) { session.AddReference(assembly); }
        public void AddReference(string assemblyDisplayNameOrPath) { session.AddReference(assemblyDisplayNameOrPath); }

        public override object Execute(string content)
        {
            return session.Execute(content);
        }

        public override T Execute<T>(string content)
        {
            return session.Execute<T>(content);
        }
    }
}
