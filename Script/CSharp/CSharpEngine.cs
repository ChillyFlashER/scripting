﻿namespace Scripting.CSharp
{
    using Roslyn.Scripting;
    using Roslyn.Scripting.CSharp;
    using System.Reflection;

    /// <summary>
    /// A Wrapper for Roslyn CSharp.
    /// </summary>
    public class CSharpEngine : Scripting.Hosting.ScriptEngine
    {
        #region Properties

        public override string Name
        {
            get { return "CSharp"; }
        }

        /// <summary>
        /// Gets all the imported namespaces.
        /// </summary>
        public string[] ImportedNamespaces
        {
            get { return scriptEngine.GetImportedNamespaces().ToArray(); }
        }

        #endregion

        #region Fields

        private ScriptEngine scriptEngine;
        private Session session;

        #endregion

        #region Constructors

        public CSharpEngine()
        {
            scriptEngine = new ScriptEngine();
            session = scriptEngine.CreateSession();
        }

        #endregion

        #region Methods

        public void AddReference(Assembly assembly)
        {
            scriptEngine.AddReference(assembly);
        }

        public void AddReference(string assemblyDisplayNameOrPath)
        {
            scriptEngine.AddReference(assemblyDisplayNameOrPath);
        }

        public void ImportNamespace(string @namespace)
        {
            scriptEngine.ImportNamespace(@namespace);
        }

        #endregion

        public override Scripting.Hosting.ScriptSession CreateSession(object hostObject)
        {
            return new CSharp.CSharpSession
            {
                engine = scriptEngine,
                session = scriptEngine.CreateSession(hostObject),
            };
        }

        public override object Execute(string content)
        {
            return session.Execute(content);
        }

        public override T Execute<T>(string content)
        {
            return session.Execute<T>(content);
        } 
    }
}
