﻿namespace Script
{
    using Roslyn.Scripting;
    using Roslyn.Scripting.VisualBasic;

    public class VisualBasic : SScript
    {
        private ScriptEngine engine;
        private Session session;

        public VisualBasic()
        {
            engine = new ScriptEngine();
            session = engine.CreateSession();
        }

        public override void CreateMethod(string content)
        {
            session.Execute(content);
        }

        public override T Run<T>(string content)
        {
            return session.Execute<T>(content);
        }

        public override T RunMethod<T>(string method, params object[] parameters)
        {
            // TODO: Parameters
            return session.Execute<T>(method);
        }
    }
}
