﻿namespace Scripting.Hosting
{
    public abstract class ScriptEngine
    {
        public abstract string Name { get; }

        public abstract ScriptSession CreateSession(object hostObject);

        public abstract object Execute(string content);
        public abstract T Execute<T>(string content);
    }
}
