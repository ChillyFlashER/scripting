﻿namespace Scripting.Hosting
{
    public abstract class ScriptSession
    {
        public abstract object Execute(string content);
        public abstract T Execute<T>(string content);
    }
}
