﻿namespace Script.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class RubyTest
    {
        Ruby script = new Ruby();

        [TestMethod]
        public void CreateMethodTest()
        {
            script.CreateMethod("def Method(value) return value end");

            Assert.AreEqual(true, script.Run<bool>("Method(true)"));
            Assert.AreEqual(false, script.Run<bool>("Method(false)"));
        }

        [TestMethod]
        public void RunTest()
        {
            Assert.AreEqual(2, script.Run<int>("1 + 1"));
        }

        [TestMethod]
        public void RunMethodTest()
        {
            script.CreateMethod("def test(value1, value2) return value1 + value2 end");

            Assert.AreEqual(2, script.RunMethod<int>("test", 1, 1));
        }
    }
}
