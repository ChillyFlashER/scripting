﻿namespace Script.Test
{
    public class Expose
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public bool CanCall()
        {
            return true;
        }
    }
}
