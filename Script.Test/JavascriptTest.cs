﻿namespace Script.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class JavascriptTest
    {
        Javascript script = new Javascript();

        string HelloWorld = string.Empty;

        [TestMethod]
        public void CreateMethodTest()
        {
            script.CreateMethod("function Method(value) { return value; }");

            Assert.AreEqual(true, script.Run<bool>("Method(true)"));
            Assert.AreEqual(false, script.Run<bool>("Method(false)"));
        }

        [TestMethod]
        public void RunTest()
        {
            Assert.AreEqual(2, script.Run<int>("1 + 1"));
        }

        [TestMethod]
        public void RunMethodTest()
        {
            script.CreateMethod("function test(value1, value2) { return value1 + value2; }");

            Assert.AreEqual(2, script.RunMethod<int>("test", 1, 1));
        }

        [TestMethod]
        public void GlobalFunctionTest()
        {
            script.SetGlobalFunction("GlobalFunctionTest", new Func<int, int, int>((a, b) => a + b));

            Assert.AreEqual(3, script.RunMethod<int>("GlobalFunctionTest", 1, 2));
        }

        [TestMethod]
        public void ExposeClassPropertiesTest()
        {
            script.SetGlobalValue("expose", new JavascriptExpose(new Expose { Name = "Javascript Test", Value = 5 }));
            // Currently the properties wont update because there are no databinding 
            // You have to use JavascriptExpose.UpdateAllProperties() or more specific JavascriptExpose.UpdateProperty(string propertyName)
            
            Assert.AreEqual("Javascript Test 5", script.Run<string>("expose.Name + ' ' + expose.Value"));
        }

        [TestMethod]
        public void ExposeClassMethodsTest()
        {
            script.SetGlobalValue("expose", new JavascriptExpose(new Expose { Name = "Javascript Test", Value = 5 }));

            Assert.AreEqual(true, script.Run<bool>("expose.CanCall()"));
        }
    }
}
