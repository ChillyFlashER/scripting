﻿namespace Scripting.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Scripting.Python;

    [TestClass]
    public class PythonTest
    {
        PythonEngine scriptEngine = new PythonEngine();

        [TestMethod]
        public void HelloWorld()
        {
            var scriptSession = scriptEngine.CreateSession(null);

            var sb1 = new System.Text.StringBuilder();
            sb1.AppendLine("class HelloWorld(object):");
            sb1.AppendLine("   def say_hello(self):");
            sb1.AppendLine("      return 'Hello World!'");

            scriptSession.Execute(sb1.ToString());

            var sb2 = new System.Text.StringBuilder();
            sb2.AppendLine("hello = HelloWorld()");
            sb2.AppendLine("hello.say_hello()");

            Assert.AreEqual("Hello World!", scriptSession.Execute<string>(sb2.ToString()));
        }

        [TestMethod]
        public void ExecuteTest()
        {
            Assert.AreEqual(2, scriptEngine.Execute<int>("1 + 1"));
        }
    }
}
