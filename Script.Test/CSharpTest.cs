﻿namespace Scripting.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Scripting.CSharp;

    [TestClass]
    public class CSharpTest
    {
        CSharpEngine scriptEngine = new CSharpEngine();

        [TestMethod]
        public void Methods()
        {
            var scriptSession = scriptEngine.CreateSession(null);

            scriptSession.Execute("bool Method(bool value) { return value; }");

            Assert.AreEqual(true, scriptSession.Execute<bool>("Method(true)"));
            Assert.AreEqual(false, scriptSession.Execute<bool>("Method(false)"));
        }

        [TestMethod]
        public void ExecuteTest()
        {
            Assert.AreEqual(2, scriptEngine.Execute<int>("1 + 1"));
        }

        [TestMethod]
        public void HostObjectTest()
        {
            var hostObject = new HostObject();
            var scriptSession = scriptEngine.CreateSession(hostObject);

            scriptEngine.AddReference(hostObject.GetType().Assembly);
            scriptEngine.AddReference("System");

            //scriptSession.Execute<string>("TextProperty = \"Hello World!\";");

            Assert.AreEqual(hostObject.TextProperty, scriptSession.Execute("TextProperty"));
        }
    }
}
