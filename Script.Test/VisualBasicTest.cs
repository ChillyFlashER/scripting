﻿namespace Script.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class VisualBasicTest
    {
        VisualBasic script = new VisualBasic();

        [TestMethod]
        public void CreateMethodTest()
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendLine("Public Function Method(ByVal value As Boolean) As Boolean");
            sb.AppendLine("   Return value");
            sb.AppendLine("End Function");

            script.CreateMethod(sb.ToString());

            Assert.AreEqual(true, script.Run<bool>("Method(True)"));
            Assert.AreEqual(false, script.Run<bool>("Method(False)"));
        }

        [TestMethod]
        public void RunTest()
        {
            Assert.AreEqual(2, script.Run<int>("1 + 1"));
        }

        [TestMethod]
        public void RunMethodTest()
        {
            script.CreateMethod("void test(int value1, int value2) { return value1 + value2; }");

            Assert.AreEqual(2, script.RunMethod<int>("test", 1, 1));
        }
    }
}
