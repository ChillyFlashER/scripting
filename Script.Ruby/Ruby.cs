﻿namespace Script
{
    using Microsoft.Scripting.Hosting;

    public class Ruby : SScript
    {
        private ScriptEngine engine;
        private ScriptScope scope;

        public Ruby()
        {
            engine = IronRuby.Ruby.CreateEngine();
            scope = engine.CreateScope();
        }

        public override void CreateMethod(string content)
        {
            engine.Execute(content);
        }

        public override T Run<T>(string content)
        {
            return engine.Execute<T>(content);
        }

        public override T RunMethod<T>(string method, params object[] parameters)
        {
            // TODO: Parameters
            return engine.Execute<T>(method);
        }
    }
}
